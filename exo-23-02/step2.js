const add = (string) => {
	if (string === "") { return 0 }
	const numbers = string.split(",");
	let sum = 0;
	numbers.forEach((number) => {
		sum += parseFloat(number);
	})
	return sum.toFixed(4).replace(/\.0*$|(\.\d*[1-9])0+$/, '$1').toString()
}

console.log(add('1.1,2.2'))
console.log(add('4,2,311'))
console.log(add('1,2,3'))
console.log(add('1,2,3'))
console.log(add('1,2,3'))
