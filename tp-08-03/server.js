
const express = require('express');
const app = express();
require('express-ws')(app);
const serveStatic = require('serve-static');

let clients = [];
let messages = [];

app.use(serveStatic(`${__dirname}/public`));

app.set('port', process.env.PORT || 3000);


app.ws('/', (ws, req) => {
    ws.on('message', message => {
        console.log('Received -', message);
        const messageObject = JSON.parse(message);
        switch (messageObject.action) {
            case "new_message":
                messages.push(message);
                clients.forEach((socket, index) => {
                    if (socket.readyState === 1 ) {
                        socket.send(message);
                    } else if (socket.readyState === 2 || socket.readyState === 3) {
                        clients.splice(index, 1)
                    }
                });
                break;
            case "connect":
                clients.push(ws);
                messages.forEach(message => {
                    ws.send(message);
                });
                break;
            case "disconnect":
                console.log("client disonnected, REASON: ", message.disconnectReason);
                clients.splice(clients.find((testingWs) => testingWs === ws), 1);
                break;
        }
    });
});


app.listen(app.get('port'), () => {
    console.log('Server listening on port %s', app.get('port'));
});